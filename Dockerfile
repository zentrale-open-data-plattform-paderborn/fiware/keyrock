# Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh
# This file is covered by the EUPL license.
# You may obtain a copy of the licence at
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

FROM fiware/idm:7.8.1

COPY config/config.js /opt/fiware-idm/
COPY config/docker-entrypoint.sh /opt/fiware-idm/

#Keyrock confirmation email feature
COPY config/users.js /opt/fiware-idm/controllers/web/
COPY config/activate.ejs /opt/fiware-idm/views/templates/email/

RUN chmod 775 /opt/fiware-idm/docker-entrypoint.sh