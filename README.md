# Keyrock IDM

## General
Identity manager (keyrock) is configured and needed for initial user account creation.

The API management has a plugin which allows users to use Keyrock accounts to login. Keyrock (as IDM) has the notion of Organisations and Applications. When the API management is configured with an Keyrock application, this allows login to the API management with keyrock accounts. This (or separate) application can also be used to generate Oauth2 tokens to control the Tenant access. Further more, Tenant manager uses keyrock to fetch information on the users. API umbrella can (when a request with Oauth bearer token comes in) then send the token to Keyrock for introspection.

Original documentation can be found here: https://fiware-idm.readthedocs.io/en/latest/


## Usage of Keyrock
Keyrock is used to maintain user accounts and supply Bearer tokens via an API.  
Learn more from the official documentation: https://fiware-idm.readthedocs.io/en/7.8.1/oauth/introduction/index.html

Users need to sign up and wait for account approval.  
Learn more from the official documentation: https://fiware-idm.readthedocs.io/en/7.8.1/user_and_programmers_guide/user_guide/index.html  

After this, admin can authorize them to various [registered application](https://fiware-idm.readthedocs.io/en/7.8.1/user_and_programmers_guide/application_guide/index.html#register-an-application) allowing them to login (for example Knowage and APInf Platform) to 3rd party applications.  
Learn more from the official documentation: https://fiware-idm.readthedocs.io/en/7.8.1/user_and_programmers_guide/application_guide/index.html#authorize-users-and-organizations

Or you can watch this Youtube-Video: https://www.youtube.com/watch?v=nVP5P_oFC1o

## Versioning  
Tested on:  
Version fiware/idm:7.8.1 (DockerHub digest: sha256:47f7001e99428dfb89526fe5f8f255d9568e8bfd6e91076889e0db58264cd0ba)

## Volume Usage
This component does not use persistent volumes.

## Load-Balancing
Currently only one Replica is deployed.

## Route (DNS)
Keyrock UI can be accessed via: https://accounts.fiware.opendata-CITY.de

## Access the UI
The Administration User is created during deployment (see k8s secret below).
This Administration User can create other users including other administrators.

## Further Testing
The image is taken as is. No further component testing is done.

## Deployment
In order to deploy, following Gitlab-Variables and K8s-secrets are needed:
### Gitlab-Variables
GITLAB_ACCESS_TOKEN - This variable contains the token used to access Gitlab Docker image registry  
KEYROCK_DEV_DOMAIN - This variable contains the domain name used for the component in dev-stage.  
KEYROCK_PROD_DOMAIN - This variable contains the domain name used for the component in production-stage.  
KEYROCK_STAGING_DOMAIN - This variable contains the domain name used for the component in staging-stage.  
KUBECONFIG - This variable contains the content of the Kube.cfg used to access the cluster for dev- and staging-stage.  
KUBECONFIG_PROD - This variable contains the content of the Kube.cfg used to access the cluster for prod-stage.  
NAMESPACE_DEV - This variable contains the namespace for dev-stage used by k8s  
NAMESPACE_STAGING - This variable contains the namespace for staging-stage used by k8s  
NAMESPACE_PROD - This variable contains the namespace for production-stage used by k8s  

ADMINISTRATOR_EMAIL_DEV - Administrator email for added feature. Confirmation email for new keyrock accounts in DEV-namespace will be directed to this email.  
ADMINISTRATOR_EMAIL_STAGING - Administrator email for added feature. Confirmation email for new keyrock accounts in STAGING-namespace will be directed to this email.  
ADMINISTRATOR_EMAIL_PROD - Administrator email for added feature. Confirmation email for new keyrock accounts in PROD-namespace will be directed to this email.  

### Kubernetes Secrets
Create kubernetes-secret for admin email and password  
`kubectl create secret generic idm-secret --from-literal=email=YOUREMAIL --from-literal=password=YOURPASSWORD --namespace=fiware-dev|fiware-staging|fiware-prod`  

Create kubernetes-secret for email service credentials  
`kubectl create secret generic email-secret --from-literal=host=EMAILHOST --from-literal=user=EMAILUSER --from-literal=password=EMAILPASSWORD --namespace=fiware-dev|fiware-staging|fiware-prod`  

Kubernetes-secret for MYSQL database credentials, provided by MYSQL service provider.  
Kubernetes secret `db-secret` should include keys and values to match:  
`USER` - database username  
`PASSWORD` - database password for that user  

## Funding Information
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh

This work is licensed under the EUPL 1.2. See [LICENSE](LICENSE) for additional information.
